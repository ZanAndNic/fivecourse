=comment
Created by Андрей on 15.11.2016.
Скрипт поиска текста в файлах  указанного каталога и его подкаталогов.
Имена файлов, в которых обнаружено совпадение записываеюся в файл outputFile.txt

author Заварин А.Н.
version 2.0

=cut

#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

my $dir = "../searchText/files";
my $textSearch = "hello";

open OUT_FILE,">>outputFile.txt", or die "Ошибка открытия файла outputFile.txt:$!";

sub searchText{
    my ($root)=$_[0];
    my $dirFile;

    opendir ROOT, $root;
    my (@filelist) = readdir ROOT;
    closedir ROOT;

    for my $fileName (@filelist) {
        next if $fileName =~ /^\.\.?$/;

        $dirFile = $root."/".$fileName;
        print "$dirFile\n" if (-f $dirFile);

        if (-d $dirFile) {
            print "$dirFile:\n";
            searchText($dirFile);
        }
        else{
            open F1,"<".$dirFile or die "Ошибка открытия файла $dirFile:$!";
            @_=<F1>;
            close F1 or die $!;

            $_ = join"", @_;
            if($_ =~ m/$textSearch/gi){
                print "Совпадение найдено! См. файл: $fileName. Полный путь к файлу: $dirFile \n";
                print OUT_FILE "Текст поиска: $textSearch, найдено в файле: $fileName, полный путь к файлу:  $dirFile \n";
            }
        }
    }
}

searchText($dir);
close OUT_FILE;