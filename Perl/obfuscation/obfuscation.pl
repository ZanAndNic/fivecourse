=comment
Created by Андрей on 18.09.2016.
Скрипт замены идентификаторов в исходном коде на односимвольные или
 двухсимвольные имена в зависимости от количества найденных идентификаторов в коде.

author Заварин А.Н.
version 1.0

=cut

#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

open F1,"<Main.java" or die "Ошибка открытия файла Main.java:$!";
@_=<F1>;
close F1 or die $!;
open F1,">Main.java" or die "Ошибка открытия файла Main.java:$!";

my @a = ('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
my %hashIdentifiers = ();
my %hashClass = ();
my %hashMethods = ();
my @keyword = ('main','byte','short','int','long','char','float','double','boolean','if','else','switch','case','default','while','do','break','continue','for',
    'try','catch','finally','throw','throws','private','protected','public','import','package','class','interface','extends','implements','static',
    'final','void','abstract','native','new','return','this','super','synchronized','volatile','const','goto','instanceof','enum','assert','transient','strictfp');
#my my $dir = 'D:\Learning\RepositoryFiveCourse\Perl\obfuscation';
my $dir = '../obfuscation';
my @files;
my $status;

my $string = join"", @_;
#Удаление комментариев
$string =~ s/\/\/.*\n*?//g;
$string =~ s/\/\*.*?\*\///gs;
#Удаление символов новой строки и пробелов
#$string =~ tr/\n//d;
#$string =~ s/\s{2,}//g;

#Массив идентификаторов [^\.]\b([_a-z]+\w*)\b[^\{\(\.]
#[^\.]\b([_a-z]+\w*)\b[^\{\(\.]
#(?:(?:[^.])|(?:\w*))\b([_a-z]+\w*)\b[^\{\(\.]
#TODO переменные со значением типа 'dfdf'
my @identifiers = $string =~ m/[^.]\b([_a-z]+\w*)\b[^".]/gi;

%hashIdentifiers = map {$_ => ""} @identifiers;

print "Масcив идентификаторов:\n";
foreach my $el (keys %hashIdentifiers)
{
    print $el."\n";
}

#Массив классов
my @class = $string =~ m/\s{1,}\b([A-Z]+\w*)\b\s*\{/g;
%hashClass = map {$_ => ""} @class;

print "Масcив классов:\n";
foreach my $el (keys %hashClass)
{
    print $el."\n";
}

my @methods = $string =~ m/\s{1,}\b([a-z]+\w*)\b\(/g;
%hashMethods = map {$_ => ""} @methods;
print "Масcив методов:\n";
foreach my $el (keys %hashMethods)
{
    print $el."\n";
}

#Метод удаление элементов из массива
sub deletElements($ \@){
    my ($el) = $_[0];
    my (@mas) = @{$_[1]};

    foreach my $el2 (@mas){
        if($el eq $el2){
            delete($hashIdentifiers{$el});
        }
    }
}

#Удаление индентификаторов классов, методов и ключевых слов
#TODO оптимизировать ГЛАВНАЯ ФУ_Я main ??!
foreach my $el (keys %hashIdentifiers)
{
    deletElements($el, @keyword);
    deletElements($el, @class);
    deletElements($el, @methods);
}

#Удаление ключевых слов из массива методов
foreach my $el (keys %hashMethods)
{
    foreach my $el2 (@keyword){
        if($el eq $el2){
            delete($hashMethods{$el});
        }
    }
}

#Замена идентификаторов на 2-х символьные имена
#TODO разбить на методы
my $countForId = 2;
foreach my $el (keys %hashIdentifiers)
{
    $hashIdentifiers{$el} = join('', map((@a)[rand(26)], (1..$countForId)));
    foreach my $el2 (%hashIdentifiers)
    {
        if($hashIdentifiers{$el} eq $el2){
            $countForId++;
            $hashIdentifiers{$el} = join('', map((@a)[rand(26)], (1..$countForId)));
        }
    }
    $string =~ s/\b$el\b/$hashIdentifiers{$el}/g;
}
#Замена имен методов на 4-ти символьные имена
my $countForMet = 4;
foreach my $el (keys %hashMethods)
{
    $hashMethods{$el} = join('', map((@a)[rand(26)], (1..$countForMet)));
    foreach my $el2 (%hashMethods)
    {
        if($hashMethods{$el} eq $el2){
            $countForMet++;
            $hashMethods{$el} = join('', map((@a)[rand(26)], (1..$countForMet)));
        }
    }
    $string =~ s/\b$el\b/$hashMethods{$el}/g;
}

#Замена имен класов на 5-ти символьные имена
my $countForClass = 4;
foreach my $el (keys %hashClass)
{
    $hashClass{$el} = join('', map((@a)[rand(26)], (1..$countForClass)));
    foreach my $el2 (%hashClass)
    {
        if($hashClass{$el} eq $el2){
            $countForClass++;
            $hashClass{$el} = join('', map((@a)[rand(26)], (1..$countForClass)));
        }
    }
    $string =~ s/\b$el\b/$hashClass{$el}/g;
}

print F1 $string;
close F1 or die $!;

#Получение файлов .java из деректории
opendir(DIR, $dir) or die "Ошибка! $dir: $!";
while (defined(my $file = readdir(DIR))){
    next if $file =~ /^\.\.?$/;
    next if $file =~ /.*\.[^java]/;
    push @files, $file;
}
closedir(DIR);

#Замена имени файла класса
foreach my $el (keys %hashClass)
{
    foreach my $el2 (@files)
    {
        if($el.".java" eq $el2){
            $status = rename "$el.java", "$hashClass{$el}.java";
            print "Имя файла $el.java успешно изменено на $hashClass{$el}.java if $status";
        }
    }

}