=comment
Created by Андрей on 16.09.2016.
Скрипт поиска всех идентификаторов в исходной строке.

author Заварин А.Н.
version 1.0
=cut

#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

$_ = "if(name.length() > 6){
            switch (name.substring(0, 6)){
                case \"\$GNRMC\":
                case \"\$GPRMC\":
                    runRMC(name);
                    break;
                case \"\$GNGGA\":
                case \"\$GPGGA\":
                    runGGA(name);
                    break;
            }
            saveLogs();
        }";
my @mas = m/\b[_a-z]+\w*\b/gi;

foreach my $el (@mas)
{
    print $el;
    print "\n";
}