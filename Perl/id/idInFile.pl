=comment
Created by Андрей on 16.09.2016.
Скрипт поиска всех идентификаторов в файле
исходного кода на произвольном языке программирования и
сохранения результата поиска в текстовый файл

author Заварин А.Н.
version 1.0
=cut

#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

open F1,"in.java" or die "Ошибка открытия файла in.java:$!";
open F2,">out.java" or die "Ошибка открытия файла out.java:$!";

@_=<F1>;
close F1 or die $!;

print "Содержание входного файла:\n";
print @_;

my $MyString = join"",@_;
@_ = $MyString =~ m/\b[_a-z]+\w*\b/gi;

print "\nИдентификаторы:\n";
print F2 "Идентификаторы:\n";
foreach my $e (@_)
{
    print $e."\n";
    print F2 $e."\n";
}

close F2 or die $!;