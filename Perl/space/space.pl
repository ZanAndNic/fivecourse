=comment
Created by Андрей on 16.09.2016.
Скрипт удаления лишних пробелов и символов перехода на новую строку
из файла исходного кода на произвольном языке программирования и
сохранения результата удаления в файл

author Заварин А.Н.
version 1.0
=cut

#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

open F1,"in.java" or die "Ошибка открытия файла in.java:$!";
open F2,">out.java" or die "Ошибка открытия файла out.java:$!";

@_=<F1>;
close F1 or die $!;
print "До обфускации:\n";
print @_;
$_ = join"", @_;
tr/" "\n//d;

print "\nПосле обфускации:\n";
print $_;
print F2 "$_";
close F2 or die $!;